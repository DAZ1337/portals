package main

import (
    "bufio"
    "bytes"
    "encoding/binary"
    "fmt"
    "io/ioutil"
    "os"
    "strconv"
    "strings"
)

// readPortals reads portals from the data map file and outputs each portal 
// entry as a new line in the format index,x,y. All other dmap information
// will be skipped during this read.
func readPortals() {
    file, err := os.Open(argFilename)
    if err != nil { panic(err) }
    defer file.Close()
    file.Seek(268, 0)

    // Read map information to seek to portal section
    var height, width uint32
    if err = binary.Read(file, binary.LittleEndian, &width); err != nil { panic(err) }
    if err = binary.Read(file, binary.LittleEndian, &height); err != nil { panic(err) }
    file.Seek(int64(height * ((width * 6) + 4)), 1)
    if optVerbose {
        println("portals.go: height =", height)
        println("portals.go: width =", width)
    }

    // Read portals from the file
    var amount uint32
    if err = binary.Read(file, binary.LittleEndian, &amount); err != nil { panic(err) }
    if optVerbose {
        println("portals.go: amount =", amount)
    }

    // Process each portal
    for i := 0; i < int(amount); i++ {
        var x, y, index uint32
        if err = binary.Read(file, binary.LittleEndian, &x); err != nil { panic(err) }
        if err = binary.Read(file, binary.LittleEndian, &y); err != nil { panic(err) }
        if err = binary.Read(file, binary.LittleEndian, &index); err != nil { panic(err) }
        fmt.Printf("%d,%d,%d\n", index, x, y)
    }
}

// writePortals writes a list of portals to a data map file from standard
// input. Each record must be line separated and in the format index,x,y.
// All other dmap information will be copied to the new file.
func writePortals() {
    portals := make([]string, 0)
    scanner := bufio.NewScanner(os.Stdin)
    for scanner.Scan() {
        portals = append(portals, scanner.Text())
    }

    file, err := os.Open(argFilename)
    if err != nil { panic(err) }
    defer file.Close()

    fileinfo, err := file.Stat()
    if err != nil { panic(err) }
    size := fileinfo.Size()

    readBuf := make([]byte, size)
    _, err = file.Read(readBuf)
    if err != nil { panic(err) }
    mapBuf := bytes.NewBuffer(nil)
    mapBuf.Write(readBuf[0:268])

    // Read in map information and write until the portal section
    width := binary.LittleEndian.Uint32(readBuf[268:])
    height := binary.LittleEndian.Uint32(readBuf[272:])
    offset := 276 + (height * ((width * 6) + 4))
    if err = binary.Write(mapBuf, binary.LittleEndian, width); err != nil { panic(err) }
    if err = binary.Write(mapBuf, binary.LittleEndian, height); err != nil { panic(err) }
    mapBuf.Write(readBuf[276:offset])
    if optVerbose {
        println("portals.go: height =", height)
        println("portals.go: width =", width)
    }

    // Write out the new portal data
    originalAmount := binary.LittleEndian.Uint32(readBuf[offset:])
    offset += 4
    if optVerbose {
        println("portals.go: old amount =", originalAmount)
        println("portals.go: new amount =", len(portals))
    }

    if err = binary.Write(mapBuf, binary.LittleEndian, uint32(len(portals))); err != nil { panic(err) }
    offset += (12 * originalAmount) + 4
    for i := 0; i < len(portals); i++ {
        line := strings.Split(portals[i], ",")
        index, err := strconv.Atoi(line[0]); if err != nil { panic(err) }
        x, err := strconv.Atoi(line[1]); if err != nil { panic(err) }
        y, err := strconv.Atoi(line[2]); if err != nil { panic(err) }
        if optVerbose {
            println("portals.go: writing", portals[i])
        }

        if err = binary.Write(mapBuf, binary.LittleEndian, uint32(x)); err != nil { panic(err) }
        if err = binary.Write(mapBuf, binary.LittleEndian, uint32(y)); err != nil { panic(err) }
        if err = binary.Write(mapBuf, binary.LittleEndian, uint32(index)); err != nil { panic(err) }
    }
    
    // Write out the rest of the map to file
    mapBuf.Write(readBuf[offset:])
    ioutil.WriteFile(optOutputFile, mapBuf.Bytes(), 0)
}